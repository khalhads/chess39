/**
 * @author Karl Capili
 * @author Khaled haddara
 */

package chess;

import java.util.Scanner;
import environment.*;



public class Chess {

	public static void main(String args[]) {
		Environment Board = new Environment();
		Board.setup_board();
		Board.makeBoard();

		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		Boolean Turn = true;
		String dPlayer = "";
		String temp = "";
		int dCounter = 0;
		String lmove = "a0 a7";
		Boolean staleMate = false;
		Boolean wKCheck = false;
		Boolean bKCheck = false;

		while (true) {
			String Player;
			staleMate = isStaleMate();
			Player = Turn == true ? "White's Move: " : " Black's Move: ";

			if (Turn && wKCheck && staleMate) {
				System.out.println("Check Mate: Black Wins");
				break;
			} else if (!Turn && bKCheck && staleMate) {
				System.out.println("Check Mate: White Wins");
				break;
			}

			if (staleMate == true) {
				System.out.println("Stale Mate, Draw!");
				break;
			}

			System.out.println(Player);
			String move = in.nextLine();

			String response = Rules.make_move(move, Turn, lmove);
			if (response != null) {
				if (response.equals("resign")) {
					break;
				} else if (response.equals("draw")) {
					System.out.println("draw");
					dPlayer = Player;
					if (dPlayer.equals(temp)) {
						dCounter = 0;
					}
					temp = dPlayer;
					dCounter++;
					if (dCounter == 2) {
						break;
					}
				}
			}
			Board.makeBoard();
			if (black_king_checker() > 0) {
				for (int i1 = 0; i1 < 8; i1++) {
					for (int j1 = 0; j1 < 8; j1++) {
						if (Environment.pieces[j1][i1] instanceof King) {
							King bK = (King) Environment.pieces[j1][i1];
							bK.setCheck(true);
							bKCheck = true;
						}
					}
				}
			} else {
				bKCheck = false;
			}

			if (white_king_checker() > 0) {
				for (int i1 = 0; i1 < 8; i1++) {
					for (int j1 = 0; j1 < 8; j1++) {
						if (Environment.pieces[j1][i1] instanceof King) {
							King wK = (King) Environment.pieces[j1][i1];
							wK.setCheck(true);
							wKCheck = true;
						}
					}
				}
			} else {
				wKCheck = false;
			}

			if (bKCheck == true) {
				System.out.println("check");
			} else if (wKCheck == true) {
				System.out.println("check");
			}

			if (Turn == true && response != null) {
				Turn = false;
				if (move.equals("draw?")) {

				} else if (move.equals("draw")) {

				} else {
					lmove = move;
				}
			} else if (response != null) {
				Turn = true;
				if (move.equals("draw?")) {

				} else if (move.equals("draw")) {

				} else {
					lmove = move;
				}
			}
		}
		System.out.println("Good Game");
	}

	/*
	 * method is used to determine if a stalemate is present
	 */
	private static Boolean isStaleMate() {
		int counter = 0;
		int counter1 = 0;
		String color = "gray";
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (Environment.pieces[j][i] != null) {
					Piece piece = Environment.pieces[j][i];
					color = piece.getColor();
					int response = piece.movePossible(piece);

					if (response == 1 && color.equals("white")) {
						counter++;
					} else if (response == 1) {
						counter1++;
					}
					if (color.equals("white")) {
						if (counter == Environment.whtNum) {
							return true;
						}

					} else {
						if (counter1 == Environment.blkNum) {
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	/*
	 * method is used to see if the black king is in check
	 */
	public static int black_king_checker() {
		int count = 0;
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (Environment.pieces[j][i] != null) {
					if (Environment.pieces[j][i].getColor().equals("white")) {
						if (Environment.pieces[j][i].checkable(Environment.pieces[j][i], "bK") == 1) {
							count++;
						}
					}
				}
			}
		}
		return count;
	}

	/*
	 * method is used to see if white king is in check
	 */
	public static int white_king_checker() {
		int count = 0;
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (Environment.pieces[j][i] != null) {
					if (Environment.pieces[j][i].getColor().equals("black")) {
						if (Environment.pieces[j][i].checkable(Environment.pieces[j][i], "wK") == 1) {
							count++;
						}
					}
				}
			}
		}
		return count;
	}

}
