/**
 * @author Karl Capili
 * @author Khaled haddara
 */

package chess;

import static environment.Environment.pieces;
import environment.*;


/*
 * This class is to make sure that any moves made by the players are allowed
 */
public class Rules {
	String last;

	/*
	 * The get_X_position() method is used to translate the input string (letter part) into a corresponding integer that
	 * relays the x-axis position of the chess piece
	 */
	public static int get_X_position(String input) {
		int x;
		switch (input.charAt(0)) {
		case 'a':
			x = 0;
			break;
		case 'b':
			x = 1;
			break;
		case 'c':
			x = 2;
			break;
		case 'd':
			x = 3;
			break;
		case 'e':
			x = 4;
			break;
		case 'f':
			x = 5;
			break;
		case 'g':
			x = 6;
			break;
		case 'h':
			x = 7;
			break;
		default:
			x = -100;
		}
		return x;
	}

	/*
	 * The get_X_position method is used to translate the input string (number part) into a corresponding integer that
	 * relays the x-axis position of the chess piece
	 */

	public static int get_Y_position(String input) {
		int y;

		switch (input.charAt(1)) {
		case '1':
			y = 0;
			break;
		case '2':
			y = 1;
			break;
		case '3':
			y = 2;
			break;
		case '4':
			y = 3;
			break;
		case '5':
			y = 4;
			break;
		case '6':
			y = 5;
			break;
		case '7':
			y = 6;
			break;
		case '8':
			y = 7;
			break;
		default:
			y = -100;
		}
		return y;
	}


	/*
	 * This method is used to first check if the input move is valid
     * and then perform the move on the particular chess piece.
	 */
	public static String make_move(String input, Boolean turn, String lt) {
		String[] in = input.split(" ");
		String[] lin = lt.split(" ");
		int x = 0, bx = 0;
		int y = 0, by = 0;
		int lx = 0, llx = 0;
		int ly = 0, lly = 0;
		int check;

		Piece piece = new Piece();
		Piece pawnpiece = new Piece();
		if (in.length < 2) {
			if (in[0].toLowerCase().equals("draw?") || (in[0].toLowerCase().equals("draw"))) {
				return "draw";
			} else if (in[0].toLowerCase().equals("resign")) {
				return "resign";
			} else {
				System.out.println("Need More Than 1 Argument");
				return null;
			}
		}

		if (in[0].length() != 2 || in[1].length() != 2)
		{
			System.out.println("ERROR: Invalid Input");
			return null;
		}

		for (int i = 0; i < in.length; i++) {
			if (i == 0) {
				bx = get_X_position(in[i]);
				by = get_Y_position(in[i]);
				llx = get_X_position(lin[i]);
				lly = get_Y_position(lin[i]);
				if (bx >= 0 && bx <= 7 && by >= 0 && by <= 7) {
					piece = pieces[bx][by];
					if (piece != null) {
						if (piece.getColor().toLowerCase().equals("black") && turn == true) {
							System.out.println("Black Piece cannot be moved");
							return null;
						} else if (piece.getColor().toLowerCase().equals("white") && turn == false) {
							System.out.println("White Piece cannot be moved");
							return null;
						}
					}
				} else
				{
					System.out.println("Move not allowed, please try again");
					return null;
				}
			} else if (i == 1) {
				x = get_X_position(in[i]);
				y = get_Y_position(in[i]);
				lx = get_X_position(lin[i]);
				ly = get_Y_position(lin[i]);
				if (piece == null) {
					System.out.println("There is no Piece in the spot you have selected");
					return null;
				}
				if (Math.abs(lx - llx) == 0 && Math.abs(ly - lly) == 2) {
					pawnpiece = pieces[lx][ly];
					if (pawnpiece != null) {

						if (pawnpiece.getPiece().equals("bp")) {

							if (piece.getPiece().equals("wp")) {

								if (Math.abs(x - lx) == 0 && (y - ly) == 1) {

									pieces[piece.getX()][piece.getY()] = null;
									pieces[x][y] = piece;
									pieces[lx][ly] = null;
									piece.setX(x);
									piece.setY(y);
									System.out.println("EN PASSANT YOU PEASENT");
									return "done";
								}
							}
						} else if (pawnpiece.getPiece().equals("wp")) {

							if (piece.getPiece().equals("bp")) {

								if (Math.abs(x - lx) == 0 && (y - ly) == -1) {



									pieces[piece.getX()][piece.getY()] = null;
									pieces[x][y] = piece;
									pieces[lx][ly] = null;
									piece.setX(x);
									piece.setY(y);
									System.out.println("EN PASSANT YOU PEASENT");
									return "done";
								}
							}
						}
					}
				}
				if (x >= 0 && x <= 7 && y >= 0 && y <= 7) {
					check = piece.movePiece(piece, x, y);

					switch (check) {
					case (0):
						break;
					case (-1):
						return null;
					case (2):
						if (in.length == 3) {
							if (in[2].toLowerCase().equals("n")) {
								Knight wN = new Knight("white", "wN", x, y);
								pieces[x][y] = wN;
								break;
							} else if (in[2].toLowerCase().equals("r")) {
								Rook wR = new Rook("white", "wR", x, y, true);
								pieces[x][y] = wR;
								break;
							} else if (in[2].toLowerCase().equals("b")) {
								Bishop wB = new Bishop("white", "wB", x, y);
								pieces[x][y] = wB;
								break;
							}
						} else {
							Queen wQ = new Queen("white", "wQ", x, y);
							pieces[x][y] = wQ;
							break;
						}
					case (3):
						if (in.length == 3) {
							if (in[2].toLowerCase().equals("n")) {
								Knight bN = new Knight("black", "bN", x, y);
								pieces[x][y] = bN;
								break;
							} else if (in[2].toLowerCase().equals("r")) {
								Rook bR = new Rook("black", "bR", x, y, true);
								pieces[x][y] = bR;
								break;
							} else if (in[2].toLowerCase().equals("b")) {
								Bishop bB = new Bishop("black", "bB", x, y);
								pieces[x][y] = bB;
								break;
							}
						} else {
							Queen bQ = new Queen("black", "bQ", x, y);
							pieces[x][y] = bQ;
							break;
						}
					}
				} else {
					System.out.println("Selection Out of Bonds");
					return null;
				}
			}
		}
		return "done";
	}
}
