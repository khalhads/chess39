/**
 * @author Karl Capili
 * @author Khaled haddara
 */

package environment;

import chess.Chess;

/**
 *
 *
 *Bishop class is a subclass of the Piece class where we implement the methods and rules that
 *are specific to bishop pieces
 *
 *@param color // the color of the piece
 *@param piece // the type of piece it is
 *@param x_position
 *@param y_position
 *
 *
 */

public class Bishop extends Piece {
	String piece, color;
	int x_position, y_position;

	public Bishop(String color, String piece, int x_position, int y_position) {
		super(color, piece, x_position, y_position);
	}


	/**
	 * MovePiece() method first checks if the move is valid and then performs the move
	 *
	 * @param piece // the piece that is doing the moving
	 * @param x // the x_position where the piece wants to move to
	 * @param y // the y_position where the piece wants to move to
	 *
	 * @return int if the move is successful 0 is returned if it is invalid -1 is returned
	 */

	@Override
	public int movePiece(Piece piece, int x, int y) {
		boolean move = Math.abs(x - piece.getX()) == Math.abs(y - piece.getY());
		Piece temp = pieces[x][y];
		int O_x = piece.getX();
		int O_y = piece.getY();

		if (!move) {
			System.out.println();
			System.err.println("ERROR: Invalid Move");
			return -1;
		} else { // Valid Move Lets Check for anything in the way
			String color = piece.getColor().toLowerCase();
			boolean notvalid = false;
			int Dx = x - piece.getX(); // Need to see if Delta X is pos or neg
										// for movement
			int Dy = y - piece.getY(); // Need to see if Delta Y is pos or neg
										// for movement

			if (pieces[x][y] != null) {
				String color2 = pieces[x][y].getColor().toLowerCase();
				notvalid = color.equals(color2); // if same color not valid
			}

			if (!notvalid) {

				if (Dx < 0 && Dy < 0) { // Move Diagnol down left
					for (int i = 1; i < Math.abs(Dx); i++) {
						if (pieces[piece.getX() - i][piece.getY() - i] != null) {
							System.out.println();
							System.err.println("ERROR: Invalid Move");
							return -1;
						}
					}
				} else if (Dx < 0 && Dy > 0) { // Move Diagnol up left
					for (int i = 1; i < Math.abs(Dx); i++) {
						if (pieces[piece.getX() - i][piece.getY() + i] != null) {
							System.out.println();
							System.err.println("ERROR: Invalid Move");
							return -1;
						}
					}
				} else if (Dx > 0 && Dy < 0) { // Move Diagnol down right
					for (int i = 1; i < Math.abs(Dx); i++) {
						if (pieces[piece.getX() + i][piece.getY() - i] != null) {
							System.out.println();
							System.err.println("ERROR: Invalid Move");
							return -1;
						}
					}
				} else if (Dx > 0 && Dy > 0) { // Move Diagnol up right
					for (int i = 1; i < Math.abs(Dx); i++) {
						if (pieces[piece.getX() + i][piece.getY() + i] != null) {
							System.out.println();
							System.err.println("ERROR: Invalid Move");
							return -1;
						}
					}
				} else {
					System.out.println("Mistake");
				}
				pieces[piece.getX()][piece.getY()] = null;
				pieces[x][y] = piece;
				piece.setX(x);
				piece.setY(y);
			}

			else {
				System.out.println();
				System.err.println("ERROR: Invalid Move");
				return -1;
			}
		}
		String color = piece.getColor().toLowerCase();

		if (color.equals("white")) {
			if (Chess.white_king_checker() > 0) {
				System.out.println("Invalid Move: Your king is in check");
				pieces[O_x][O_y] = piece;
				piece.setX(O_x);
				piece.setY(O_y);
				pieces[x][y] = temp;
				return -1;
			}
		} else {
			if (Chess.black_king_checker() > 0) {
				System.out.println("Invalid Move: Your king is in check");
				piece.setX(O_x);
				piece.setY(O_y);
				pieces[O_x][O_y] = piece;
				pieces[x][y] = temp;
				return -1;
			}
		}
		if (temp != null) {
			if (temp.getColor().toLowerCase().equals("white")) {
				whtNum--;
			} else {
				blkNum--;
			}
		}
		return 0;
	}

	/**
	 * checkable() is the method used to tests if the current piece puts the opposing king into check
	 * @param piece \\ piece being tested for possible check of opposing player's king.
	 * @param king \\ the string value for what the king is
	 * @return int \\ returns 0 if the opposing king is not being checked and 1 if it is.
	 */


	@Override
	public int checkable(Piece piece, String king) {
		for (int i = 0; i < 4; i++) {
			switch (i) {
			case 0:
				int x = 1;
				int y = 1;
				while (piece.getX() - x >= 0 && piece.getY() + y < 8) {
					if (pieces[piece.getX() - x][piece.getY() + y] != null
							&& !pieces[piece.getX() - x][piece.getY() + y].piece.equals(king)) {
						break;
					} else if (pieces[piece.getX() - x][piece.getY() + y] != null
							&& pieces[piece.getX() - x][piece.getY() + y].piece.equals(king)) {
						return 1;
					}
					x++;
					y++;
				}
			case 1:
				int x1 = 1;
				int y1 = 1;
				while (piece.getX() + x1 < 8 && piece.getY() + y1 < 8) {
					if (pieces[piece.getX() + x1][piece.getY() + y1] != null
							&& !pieces[piece.getX() + x1][piece.getY() + y1].piece.equals(king)) {
						break;
					} else if (pieces[piece.getX() + x1][piece.getY() + y1] != null
							&& pieces[piece.getX() + x1][piece.getY() + y1].piece.equals(king)) {
						return 1;
					}
					x1++;
					y1++;
				}
			case 2:
				int x2 = 1;
				int y2 = 1;
				while (piece.getX() - x2 >= 0 && piece.getY() - y2 >= 0) {
					if (pieces[piece.getX() - x2][piece.getY() - y2] != null
							&& !pieces[piece.getX() - x2][piece.getY() - y2].piece.equals(king)) {
						break;
					} else if (pieces[piece.getX() - x2][piece.getY() - y2] != null
							&& pieces[piece.getX() - x2][piece.getY() - y2].piece.equals(king)) {
						return 1;
					}
					x2++;
					y2++;
				}
			case 3:
				int x3 = 1;
				int y3 = 1;
				while (piece.getX() + x3 < 8 && piece.getY() - y3 >= 0) {
					if (pieces[piece.getX() + x3][piece.getY() - y3] != null
							&& !pieces[piece.getX() + x3][piece.getY() - y3].piece.equals(king)) {
						break;
					}
					if (pieces[piece.getX() + x3][piece.getY() - y3] != null
							&& pieces[piece.getX() + x3][piece.getY() - y3].piece.equals(king)) {
						return 1;
					}
					x3++;
					y3++;
				}
			}
		}
		return 0;
	}

	/**
	 * movePossible() is the method to test if the piece is allowed to move to fix a check. If there are no pieces
	 * that can move then it is checkmate
	 *
	 * @param piece \\ the piece being checked for a possible move.
	 * @return int \\ returns 0 if the piece can move and 1 if it cannot.
	 *
	 */
	@Override
	public int movePossible(Piece piece) {
		int x = piece.getX();
		int y = piece.getY();
		String color = piece.getColor();
		int RValue = 0;
		Piece attack[] = new Piece[8];
		int z = 0;

		if (color.equals("white")) {
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					if (Environment.pieces[j][i] != null) {
						if (Environment.pieces[j][i].getColor().equals("black")) {
							if (Environment.pieces[j][i].checkable(Environment.pieces[j][i], "wK") == 1) {
								attack[z] = pieces[j][i];
								z++;
							}
						}
					}
				}
			}
		} else {
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					if (Environment.pieces[j][i] != null) {
						if (Environment.pieces[j][i].getColor().equals("white")) {
							if (Environment.pieces[j][i].checkable(Environment.pieces[j][i], "bK") == 1) {
								attack[z] = pieces[j][i];
								z++;
							}
						}
					}
				}

			}
		}

		if (z > 2) {
			return 1;
		} else if (z == 1) {
			Piece attacker = attack[0];
			int ax = attacker.getX();
			int ay = attacker.getY();
			int px = piece.getX();
			int py = piece.getY();

			if (Math.abs(ax - px) == Math.abs(ay - py)) {
				int Dx = ay - py;
				int Dy = ax - px;

				if (Dx < 0 && Dy < 0) {
					for (int i = 1; i < Math.abs(Dx); i++) {
						if (pieces[piece.getX() - i][piece.getY() - i] != null) {
							return 1;
						}
					}
				} else if (Dx < 0 && Dy > 0) {
					for (int i = 1; i < Math.abs(Dx); i++) {
						if (pieces[piece.getX() - i][piece.getY() + i] != null) {
							return 1;
						}
					}
				} else if (Dx > 0 && Dy < 0) {
					for (int i = 1; i < Math.abs(Dx); i++) {
						if (pieces[piece.getX() + i][piece.getY() - i] != null) {
							return 1;
						}
					}
				} else if (Dx > 0 && Dy > 0) {
					for (int i = 1; i < Math.abs(Dx); i++) {
						if (pieces[piece.getX() + i][piece.getY() + i] != null) {
							return 1;
						}
					}
				}

				return 0;
			}
			return 1;
		}

		if (x > 0 && y > 0 && x < 7 && y < 7) {

			if (pieces[x + 1][y + 1] == null || pieces[x - 1][y - 1] == null || pieces[x + 1][y + 1] == null
					|| pieces[x - 1][y - 1] == null) {
				return 0;
			} else {
				if (pieces[x + 1][y + 1] != null) {
					String color1 = pieces[x + 1][y + 1].getColor();

					if (color.equals(color1)) {
						RValue = 1;
					} else {
						RValue = 0;
					}
				}

				if (pieces[x + 1][y - 1] != null) {
					String color1 = pieces[x + 1][y - 1].getColor();

					if (color.equals(color1)) {
						RValue = 1;
					} else {
						RValue = 0;
					}
				}
				if (pieces[x - 1][y + 1] != null) {
					String color1 = pieces[x - 1][y + 1].getColor();

					if (color.equals(color1)) {
						RValue = 1;
					} else {
						RValue = 0;
					}

				} else if (pieces[x - 1][y - 1] != null) {
					String color1 = pieces[x - 1][y - 1].getColor();

					if (color.equals(color1)) {
						RValue = 1;
					} else {
						RValue = 0;
					}
				}
			}
		}


		else if (x == 0 && y == 0) {
			if (pieces[x + 1][y + 1] == null) {
				return 0;
			}
			if (pieces[x + 1][y + 1] != null) {
				String color1 = pieces[x + 1][y + 1].getColor();

				if (color.equals(color1)) {
					RValue = 1;
				} else {
					RValue = 0;
				}
			}
		}

		else if (x == 7 && y == 0) {
			if (pieces[x - 1][y + 1] == null) {
				return 0;
			}
			if (pieces[x - 1][y + 1] != null) {
				String color1 = pieces[x - 1][y + 1].getColor();

				if (color.equals(color1)) {
					RValue = 1;
				} else {
					RValue = 0;
				}
			}
		}

		else if (x == 7 && y == 7) {
			if (pieces[x - 1][y - 1] == null) {
				return 0;
			}
			if (pieces[x - 1][y - 1] != null) {
				String color1 = pieces[x - 1][y - 1].getColor();

				if (color.equals(color1)) {
					RValue = 1;
				} else {
					RValue = 0;
				}
			}
		}

		else if (x == 0 && y == 7) {
			if (pieces[x + 1][y - 1] == null) {
				return 0;
			}
			if (pieces[x + 1][y - 1] != null) {
				String color1 = pieces[x + 1][y - 1].getColor();

				if (color.equals(color1)) {
					RValue = 1;
				} else {
					RValue = 0;
				}
			}
		}

		else if (x == 0 && y > 0 && y < 7) {
			if (pieces[x + 1][y + 1] == null || pieces[x + 1][y - 1] == null) {
				return 0;
			}
			if (pieces[x + 1][y + 1] != null) {
				String color1 = pieces[x + 1][y + 1].getColor();

				if (color.equals(color1)) {
					RValue = 1;
				} else {
					RValue = 0;
				}
			} else if (pieces[x + 1][y - 1] != null) {
				String color1 = pieces[x + 1][y - 1].getColor();

				if (color.equals(color1)) {
					RValue = 1;
				} else {
					RValue = 0;
				}
			}
		}

		else if (x == 7 && y > 0 && y < 0) {
			if (pieces[x - 1][y + 1] == null || pieces[x - 1][y - 1] == null) {
				return 0;
			}
			if (pieces[x - 1][y + 1] != null) {
				String color1 = pieces[x - 1][y + 1].getColor();

				if (color.equals(color1)) {
					RValue = 1;
				} else {
					RValue = 0;
				}
			} else if (pieces[x - 1][y - 1] != null) {
				String color1 = pieces[x - 1][y - 1].getColor();

				if (color.equals(color1)) {
					RValue = 1;
				} else {
					RValue = 0;
				}
			}
		}

		else if (x > 0 && x < 7 && y == 0) {
			if (pieces[x + 1][y + 1] == null || pieces[x - 1][y + 1] == null) {
				return 0;
			}
			if (pieces[x + 1][y + 1] != null) {
				String color1 = pieces[x + 1][y + 1].getColor();

				if (color.equals(color1)) {
					RValue = 1;
				} else {
					RValue = 0;
				}
			} else if (pieces[x - 1][y + 1] != null) {
				String color1 = pieces[x + 1][y + 1].getColor();

				if (color.equals(color1)) {
					RValue = 1;
				} else {
					RValue = 0;
				}
			}
		}

		else if (x > 0 && x < 7 && y == 7) {
			if (pieces[x + 1][y - 1] == null || pieces[x - 1][y - 1] == null) {
				return 0;
			}
			if (pieces[x + 1][y - 1] != null) {
				String color1 = pieces[x + 1][y - 1].getColor();

				if (color.equals(color1)) {
					RValue = 1;
				} else {
					RValue = 0;
				}
			} else if (pieces[x - 1][y - 1] != null) {
				String color1 = pieces[x + 1][y - 1].getColor();

				if (color.equals(color1)) {
					RValue = 1;
				} else {
					RValue = 0;
				}
			}
		}
		return RValue;
	}
}
