/**
 * @author Karl Capili
 * @author Khaled haddara
 */

package environment;


/**
 * This class serves as a superclass for the individual pieces that will be created
 *
 *@param piece // Type of piece it is (pawn, king, queen, etc.)
 *@param color // The color of the piece (black or white)
 *@param x_position
 *@param y_position
 *
 *
 */
public class Piece extends Environment
{
	String color;
	String piece;
	int x_position;
	int y_position;

	public Piece(String color, String piece, int x_position, int y_position)
	{
		this.color = color;
		this.piece = piece;
		this.x_position = x_position;
		this.y_position = y_position;
	}


	public Piece() {
		// TODO Auto-generated constructor stub
	}

	public void setColor(String color)
	{
		this.color = color;
	}

	public String getColor()
	{
		return color;
	}

	public void setPiece(String piece)
	{
		this.piece = piece;
	}

	public String getPiece()
	{
		return piece;
	}

	public void setX(int x_position)
	{
		this.x_position = x_position;
	}

	public int getX()
	{
		return x_position;
	}

	public void setY(int y_position)
	{
		this.y_position = y_position;
	}

	public int getY()
	{
		return y_position;
	}

	/**
	 * MovePiece() method first checks if the move is valid and then performs the move
	 *
	 * @param piece // the piece that is doing the moving
	 * @param x // the x_position where the piece wants to move to
	 * @param y // the y_position where the piece wants to move to
	 *
	 * @return int if the move is successful 0 is returned if it is invalid -1 is returned
	 */

	public int movePiece(Piece piece, int x, int y) {
		return 0;
	}

	/**
	 * checkable() is the method used to tests if the current piece puts the opposing king into check
	 * @param piece \\ piece being tested for possible check of opposing player's king.
	 * @param king \\ the string value for what the king is
	 * @return int \\ returns 0 if the opposing king is not being checked and 1 if it is.
	 */

	public int checkable(Piece piece, String king) {
		return 0;
	}

	/**
	 * movePossible() is the method to test if the piece is allowed to move to fix a check. If there are no pieces
	 * that can move then it is checkmate
	 *
	 * @param piece \\ the piece being checked for a possible move.
	 * @return int \\ returns 0 if the piece can move and 1 if it cannot.
	 *
	 */

	public int movePossible(Piece piece) {
		return 0;
	}
}
