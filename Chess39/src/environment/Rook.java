/**
 * @author Karl Capili
 * @author Khaled haddara
 */

package environment;

import chess.Chess;


/**
 *
 *Rook class is a subclass of the Piece class where the methods and rules that are specific to the Rook are created
 *@param piece // the type of piece it is
 *@param color // the color of the piece
 *@param x_position
 *@param y_position
 *@moved // Used to know if the rook has been moved, because castling cannot be performed once it has been moved.
 */

public class Rook extends Piece {
	String color;
	String piece;
	int x_position;
	int y_position;
	boolean moved;

	public Rook(String color, String piece, int x_position, int y_position, boolean moved) {
		super(color, piece, x_position, y_position);
		this.moved = moved;
	}


	/**
	 * MovePiece() method first checks if the move is valid and then performs the move
	 *
	 * @param piece // the piece that is doing the moving
	 * @param x // the x_position where the piece wants to move to
	 * @param y // the y_position where the piece wants to move to
	 *
	 * @return int if the move is successful 0 is returned if it is invalid -1 is returned
	 */

	@Override
	public int movePiece(Piece piece, int x, int y) {
		boolean move = false;
		Piece temp = null;
		int O_x = piece.getX();
		int O_y = piece.getY();

		if (piece.getX() - x != 0 && piece.getY() - y == 0 || piece.getX() - x == 0 && piece.getY() - y != 0) {
			move = true;
		}

		if (!move) {
			System.out.println();
			System.err.println("ERROR: Invalid Move");
			return -1;
		}

		else { // Check if okay
			String color = piece.getColor().toLowerCase();
			boolean notvalid = false;
			int Dx = x - piece.getX(); // Need to see if Delta X is pos or neg
										// movement
			int Dy = y - piece.getY(); // Need to see if Delta Y is pos or neg
										// for movement

			if (pieces[x][y] != null) {
				String color2 = pieces[x][y].getColor().toLowerCase();
				notvalid = color.equals(color2); // if same color not valid
			}

			if (!notvalid) { // Lets try to move

				if (Dx < 0 && Dy == 0) { // Move Left
					for (int i = 1; i < Math.abs(Dx); i++) {
						if (pieces[piece.getX() - i][piece.getY()] != null) {
							System.out.println();
							System.err.println("ERROR: Invalid Move");
							return -1;
						}
					}
				} else if (Dx == 0 && Dy > 0) { // Move Up
					for (int i = 1; i < Math.abs(Dy); i++) {
						if (pieces[piece.getX()][piece.getY() + i] != null) {
							System.out.println();
							System.err.println("ERROR: Invalid Move");
							return -1;
						}
					}
				} else if (Dx > 0 && Dy == 0) { // Move Right
					for (int i = 1; i < Dx; i++) {
						if (pieces[piece.getX() + i][piece.getY()] != null) {
							System.out.println();
							System.err.println("ERROR: Invalid Move");
							return -1;
						}
					}
				} else if (Dx == 0 && Dy < 0) { // Move Down
					for (int i = 1; i < Math.abs(Dy); i++) {
						if (pieces[piece.getX()][piece.getY() - i] != null) {
							System.out.println();
							System.err.println("ERROR: Invalid Move");
							return -1;
						}
					}
				} else {
					System.out.println("My bad made mistake");
				}
				temp = pieces[x][y];
				pieces[piece.getX()][piece.getY()] = null;
				pieces[x][y] = piece;
				piece.setX(x);
				piece.setY(y);
				moved = true;
			}

			else {
				System.out.println();
				System.err.println("ERROR: Invalid Move");
				return -1;
			}
		}
		String color = piece.getColor().toLowerCase();

		if (color.equals("white")) {
			if (Chess.white_king_checker() > 0) {
				System.out.println("Invalid Move: Your king is in check");
				pieces[O_x][O_y] = piece;
				piece.setX(O_x);
				piece.setY(O_y);
				pieces[x][y] = temp;
				return -1;
			}
		} else { // Black
			if (Chess.black_king_checker() > 0) {
				System.out.println("Invalid Move: Your king is in check");
				pieces[O_x][O_y] = piece;
				piece.setX(O_x);
				piece.setY(O_y);
				pieces[x][y] = temp;
				return -1;
			}
		}
		if (temp != null) {
			if (temp.getColor().toLowerCase().equals("white")) {
				whtNum--;
			} else {
				blkNum--;
			}
		}
		return 0;
	}

	public void setMoved(boolean moved) {
		this.moved = moved;
	}

	public boolean getMoved() {
		return moved;
	}


	/**
	 * checkable() is the method used to tests if the current piece puts the opposing king into check
	 * @param piece \\ piece being tested for possible check of opposing player's king.
	 * @param king \\ the string value for what the king is
	 * @return int \\ returns 0 if the opposing king is not being checked and 1 if it is.
	 */

	@Override
	public int checkable(Piece piece, String king) {
		for (int i = 0; i < 4; i++) {
			switch (i) {
			case 0:
				int x = 1;
				while (piece.getX() + x < 8) {
					if (pieces[piece.getX() + x][piece.getY()] != null
							&& !pieces[piece.getX() + x][piece.getY()].piece.equals(king)) {
						break;
					} else if (pieces[piece.getX() + x][piece.getY()] != null
							&& pieces[piece.getX() + x][piece.getY()].piece.equals(king)) {
						return 1;
					}
					x++;
				}
			case 1:
				int y = 1;
				while (piece.getY() + y < 8) {
					if (pieces[piece.getX()][piece.getY() + y] != null
							&& !pieces[piece.getX()][piece.getY() + y].piece.equals(king)) {
						break;
					} else if (pieces[piece.getX()][piece.getY() + y] != null
							&& pieces[piece.getX()][piece.getY() + y].piece.equals(king)) {
						return 1;
					}
					y++;
				}
			case 2:
				int x1 = 1;
				while (piece.getX() - x1 >= 0) {
					if (pieces[piece.getX() - x1][piece.getY()] != null
							&& !pieces[piece.getX() - x1][piece.getY()].piece.equals(king)) {
						break;
					} else if (pieces[piece.getX() - x1][piece.getY()] != null
							&& pieces[piece.getX() - x1][piece.getY()].piece.equals(king)) {
						return 1;
					}
					x1++;
				}
			case 3:
				int y1 = 1;
				while (piece.getY() - y1 >= 0) {
					if (pieces[piece.getX()][piece.getY() - y1] != null
							&& !pieces[piece.getX()][piece.getY() - y1].piece.equals(king)) {
						break;
					}
					if (pieces[piece.getX()][piece.getY() - y1] != null
							&& pieces[piece.getX()][piece.getY() - y1].piece.equals(king)) {
						return 1;
					}
					y1++;
				}
			}
		}
		return 0;
	}

	/**
	 * movePossible() is the method to test if the piece is allowed to move to fix a check. If there are no pieces
	 * that can move then it is checkmate
	 *
	 * @param piece \\ the piece being checked for a possible move.
	 * @return int \\ returns 0 if the piece can move and 1 if it cannot.
	 *
	 */
	@Override
	public int movePossible(Piece piece) {
		int x = piece.getX();
		int y = piece.getY();
		String color = piece.getColor().toLowerCase();
		Piece temp = null;
		int RValue = 0;
		Piece attack[] = new Piece[8];
		int z = 0;

		if (color.equals("white")) {
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					if (Environment.pieces[j][i] != null) {
						if (Environment.pieces[j][i].getColor().equals("black")) {
							if (Environment.pieces[j][i].checkable(Environment.pieces[j][i], "wK") == 1) {
								attack[z] = pieces[j][i];
								z++;
							}
						}
					}
				}
			}
		} else {
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					if (Environment.pieces[j][i] != null) {
						if (Environment.pieces[j][i].getColor().equals("white")) {
							if (Environment.pieces[j][i].checkable(Environment.pieces[j][i], "bK") == 1) {
								attack[z] = pieces[j][i];
								z++;
							}
						}
					}
				}

			}
		}

		if (z > 2) {
			return 1;
		} else if (z == 1) {
			Piece attacker = attack[0];
			int ax = attacker.getX();
			int ay = attacker.getY();
			int px = piece.getX();
			int py = piece.getY();
			int Dx = ax - px;
			int Dy = ay - py;

			if (ax - px != 0 && py - ay == 0) {
				 if (Dx == 0 && Dy > 0) {
					for (int i = 1; i < Math.abs(Dy); i++) {
						if (pieces[piece.getX()][piece.getY() + i] != null) {
							return 1;
						}
					}
				} else if (Dx == 0 && Dy < 0) {
					for (int i = 1; i < Math.abs(Dy); i++) {
						if (pieces[piece.getX()][piece.getY() - i] != null) {
							return 1;
						}
					}
				}

				return 0;
			} else if (ax - px == 0 && py - ay != 0) {
				if (Dx < 0 && Dy == 0) {
					for (int i = 1; i < Math.abs(Dx); i++) {
						if (pieces[piece.getX() - i][piece.getY()] != null) {
							return 1;
						}
					}
				}
				 else if (Dx > 0 && Dy == 0) {
						for (int i = 1; i < Dx; i++) {
							if (pieces[piece.getX() + i][piece.getY()] != null) {
								return 1;
							}
						}
					}

				return 0;
			}
			return 1;
		}

		if (x - 1 >= 0) {
			int a = 0;
			if (pieces[x - 1][y] != null) {
				if (pieces[x - 1][y].getColor().equals(color)) {
					RValue = 1;
					a++;
				}
			}
			temp = pieces[x - 1][y];
			pieces[x - 1][y] = piece;
			if (color.equals("white")) {
				if (Chess.white_king_checker() > 0) {
					pieces[x][y] = piece;
					pieces[x - 1][y] = temp;
					RValue = 1;
					a++;
				}
			} else {
				if (Chess.black_king_checker() > 0) {
					pieces[x][y] = piece;
					pieces[x - 1][y] = temp;
					RValue = 1;
					a++;
				}
			}
			pieces[x][y] = piece;
			pieces[x - 1][y] = temp;
			if (a == 0) {
				return 0;
			}
		}
		if (x + 1 <= 7) {
			int a = 0;

			if (pieces[x + 1][y] != null) {
				if (pieces[x + 1][y].getColor().equals(color)) {
					RValue = 1;
					a++;
				}
			}
			temp = pieces[x + 1][y];
			pieces[x + 1][y] = piece;
			if (color.equals("white")) {
				if (Chess.white_king_checker() > 0) {
					pieces[x][y] = piece;
					pieces[x + 1][y] = temp;
					RValue = 1;
					a++;
				}
			} else {
				if (Chess.black_king_checker() > 0) {
					pieces[x][y] = piece;
					pieces[x + 1][y] = temp;
					RValue = 1;
					a++;
				}
			}
			pieces[x][y] = piece;
			pieces[x + 1][y] = temp;
			if (a == 0) {
				return 0;
			}
		}

		if (y - 1 >= 0) {
			int a = 0;
			if (pieces[x][y - 1] != null) {
				if (pieces[x][y - 1].getColor().equals(color)) {
					RValue = 1;
					a++;
				}
			}
			temp = pieces[x][y - 1];
			pieces[x][y - 1] = piece;
			if (color.equals("white")) {
				if (Chess.white_king_checker() > 0) {
					pieces[x][y] = piece;
					pieces[x][y - 1] = temp;
					RValue = 1;
					a++;
				}
			} else { // Black
				if (Chess.black_king_checker() > 0) {
					pieces[x][y] = piece;
					pieces[x][y - 1] = temp;
					RValue = 1;
					a++;
				}
			}
			pieces[x][y] = piece;
			pieces[x][y - 1] = temp;
			if (a == 0) {
				return 0;
			}
		}

		if (y + 1 <= 7) {
			int a = 0;
			if (pieces[x][y + 1] != null) {
				if (pieces[x][y + 1].getColor().equals(color)) {
					RValue = 1;
					a++;
				}
			}
			temp = pieces[x][y + 1];
			pieces[x][y + 1] = piece;
			if (color.equals("white")) {
				if (Chess.white_king_checker() > 0) {
					pieces[x][y] = piece;
					pieces[x][y + 1] = temp;
					RValue = 1;
					a++;
				}
			} else {
				if (Chess.black_king_checker() > 0) {
					pieces[x][y] = piece;
					pieces[x][y + 1] = temp;
					RValue = 1;
					a++;
				}
			}
			pieces[x][y] = piece;
			pieces[x][y + 1] = temp;
			if (a == 0) {
				return 0;
			}
		}

		pieces[x][y] = piece;
		return RValue;
	}
}
